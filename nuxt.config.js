// https://nuxt.com/docs/api/configuration/nuxt-config
export default {
  modules: ["@nuxtjs/tailwindcss", "nuxt-headlessui"],
  buildModules: [
    "@nuxt/typescript-build",
    "@nuxt-hero-icons/outline/nuxt",
    "@nuxt-hero-icons/solid/nuxt",
  ],
  tailwindcss: {
    cssPath: "~/assets/css/tailwind.css",
  },
};
